export interface IAction<Type, Payload = undefined> {
    type: Type;
    payload?: Payload;
}

