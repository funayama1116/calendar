import dayjs from "dayjs";
import { CalendarAction, ActionType } from "./actions";

export const statusInit = {
  today: dayjs(),
  selectedDate: dayjs()
}
export type StatusType = typeof statusInit

// const init = formatMonth(day);

// const init = {
//   year: day.year(),
//   month: day.month() + 1
//   // +1としているのはday.month()は月情報のインデックス番号を返す。すなわち0 ~ 11の数字となるため。
// };

const calendarReducer = (state: StatusType = statusInit, action: ActionType): StatusType => {
  console.debug("reducer calendar " + action.type)
  switch (action.type) {
    case CalendarAction.SET_MONTH:
      return { ...state, ...action.payload };
    case CalendarAction.PREVIOUS_MONTH:
      return { ...state, selectedDate: state.selectedDate.subtract(1, 'month') };
    case CalendarAction.CURRENT_MONTH:
      return { ...state, selectedDate: state.today };
    case CalendarAction.NEXT_MONTH:
      return { ...state, selectedDate: state.selectedDate.add(1, 'month') };
    default:
      return state;
  }
};

export default calendarReducer;