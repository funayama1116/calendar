import { IAction } from '../../base/reducer-base'

// constants
export enum CalendarAction {
  SET_MONTH,
  PREVIOUS_MONTH,
  CURRENT_MONTH,
  NEXT_MONTH
}

export type ActionTypeSetMonth = IAction<CalendarAction.SET_MONTH, object>
export type ActionTypePreviousMonth = IAction<CalendarAction.PREVIOUS_MONTH>
export type ActionTypeCurrentMonth = IAction<CalendarAction.CURRENT_MONTH>
export type ActionTypeNextMonth = IAction<CalendarAction.NEXT_MONTH>

export type ActionType = (
  ActionTypeSetMonth |
  ActionTypePreviousMonth |
  ActionTypeNextMonth |
  ActionTypeCurrentMonth
)

// actions
export const calendarSetMonth = (payload: object): ActionType => ({
  type: CalendarAction.SET_MONTH,
  payload
});

export const calendarPreviousMonth = (): ActionType => ({
  type: CalendarAction.PREVIOUS_MONTH
});

export const calendarCurrentMonth = (): ActionType => ({
  type: CalendarAction.CURRENT_MONTH
});

export const calendarNextMonth = (): ActionType => ({
  type: CalendarAction.NEXT_MONTH
});
