import dayjs from "dayjs";
import { 
  ActionType, 
  AddScheduleAction
} from "./actions";

export const statusInit = {
  form: {
    title: "",
    description: "",
    date: dayjs(),
    location: ""
  },
  isStartEdit: false,
  isOpen: false
};
export type statusType = typeof statusInit

const addScheduleReducer = (state: statusType = statusInit, action: ActionType): statusType => {
  console.debug("func reducer add " + action.type)
  console.debug("func reducer add " + state.form.title)
  switch (action.type) {
    case AddScheduleAction.SET_VALUE:
      return { ...state, form: { ...state.form, ...action.payload } };
    case AddScheduleAction.OPEN_DIALOG:
      console.debug(action.payload?.date.toISOString())
      return { ...state, isOpen: true, form: { ...state.form, ...action.payload}};
    case AddScheduleAction.CLOSE_DIALOG:
      return statusInit;
    case AddScheduleAction.START_EDIT:
      return { ...state, isStartEdit: true };
    default:
      return state;
  }
};

export default addScheduleReducer;