import dayjs from 'dayjs'
import { IAction } from '../../base/reducer-base'

// constants
export enum AddScheduleAction {
  SET_VALUE,
  OPEN_DIALOG,
  CLOSE_DIALOG,
  START_EDIT
}

export type ActionTypeSetValue = IAction<AddScheduleAction.SET_VALUE, object>
export type ActionTypeOpenDialog = IAction<AddScheduleAction.OPEN_DIALOG, {date: dayjs.Dayjs}>
export type ActionTypeCloseDialog = IAction<AddScheduleAction.CLOSE_DIALOG>
export type ActionTypeStartEdit = IAction<AddScheduleAction.START_EDIT>

export type ActionType = (
  ActionTypeSetValue | 
  ActionTypeOpenDialog | 
  ActionTypeCloseDialog | 
  ActionTypeStartEdit
)

// actions
export const addScheduleSetValue = (payload: object): ActionType => ({
  type: AddScheduleAction.SET_VALUE,
  payload
});

export const addScheduleOpenDialog = (payload: {date: dayjs.Dayjs}): ActionType => ({
  type: AddScheduleAction.OPEN_DIALOG,
  payload
});

export const addScheduleCloseDialog = (): ActionType => ({
  type: AddScheduleAction.CLOSE_DIALOG
});

export const addScheduleStartEdit = (): ActionType => ({
  type: AddScheduleAction.START_EDIT
});