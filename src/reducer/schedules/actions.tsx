import { IAction } from '../../base/reducer-base'

// constants
export enum ScheduleAction {
  SCHEDULES_ADD_ITEM,
  SCHEDULES_FETCH_ITEM,
  SCHEDULES_DELETE_ITEM,
  SCHEDULES_SET_LOADING,
  SCHEDULES_ASYNC_FAILURE,
  SCHEDULES_RESET_ERROR
}

export type ActionTypeScheduleAddItem = IAction<ScheduleAction.SCHEDULES_ADD_ITEM, object>
export type ActionTypeScheduleFetchItem = IAction<ScheduleAction.SCHEDULES_FETCH_ITEM, object>
export type ActionTypeScheduleDeleteItem = IAction<ScheduleAction.SCHEDULES_DELETE_ITEM, object>
export type ActionTypeScheduleSetLoading = IAction<ScheduleAction.SCHEDULES_SET_LOADING>
export type ActionTypeScheduleAsyncFailure = IAction<ScheduleAction.SCHEDULES_ASYNC_FAILURE, object>
export type ActionTypeScheduleResetError = IAction<ScheduleAction.SCHEDULES_RESET_ERROR>

export type ActionType = (
  ActionTypeScheduleAddItem |
  ActionTypeScheduleFetchItem |
  ActionTypeScheduleDeleteItem |
  ActionTypeScheduleSetLoading |
  ActionTypeScheduleAsyncFailure |
  ActionTypeScheduleResetError
)

export const schedulesAddItem = (payload: object): ActionType => ({
  type: ScheduleAction.SCHEDULES_ADD_ITEM,
  payload
});

export const schedulesFetchItem = (payload: object): ActionType => ({
  type: ScheduleAction.SCHEDULES_FETCH_ITEM,
  payload
});

export const schedulesDeleteItem = (payload: object): ActionType => ({
  type: ScheduleAction.SCHEDULES_DELETE_ITEM,
  payload
});

export const schedulesSetLoading = (): ActionType => ({
  type: ScheduleAction.SCHEDULES_SET_LOADING
});

export const schedulesAsyncFailure = (payload: object) : ActionType => ({
  type: ScheduleAction.SCHEDULES_ASYNC_FAILURE,
  payload
});

export const schedulesResetError = (): ActionType => ({
  type: ScheduleAction.SCHEDULES_RESET_ERROR
});