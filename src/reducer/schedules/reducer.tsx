import dayjs from "dayjs";

import { ScheduleAction, ActionType } from "./actions";

const statusInit = {
  items: [],
  isLoading: false,
  error: null
};
type StatusType = typeof statusInit

const schedulesReducer = (state: StatusType = statusInit, action: ActionType) => {

  switch (action.type) {
    case ScheduleAction.SCHEDULES_SET_LOADING:
      return {
        ...state,
        isLoading: true
      };
    case ScheduleAction.SCHEDULES_ADD_ITEM:
      return {
        ...state,
        isLoading: false,
        items: [...state.items, action.payload]
      };
    case ScheduleAction.SCHEDULES_FETCH_ITEM:
      return {
        ...state,
        isLoading: false,
        items: action.payload
      };
    case ScheduleAction.SCHEDULES_DELETE_ITEM:
      return {
        ...state,
        isLoading: false,
        items: action.payload
      };
    case ScheduleAction.SCHEDULES_ASYNC_FAILURE:
      return {
        ...state,
        error: action.payload
      };
    case ScheduleAction.SCHEDULES_RESET_ERROR:
      return {
        ...state,
        error: null
      };
    default:
      return state;
  }
};

export default schedulesReducer;