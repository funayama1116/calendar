import dayjs from "dayjs";

type YearMonthType = {year: number, month: number}

const getMonthStateCreator = (diff: number) => (yearMonth: YearMonthType) => {
  const day = getMonth(yearMonth).add(diff, "month");
  return formatMonth(day);
};

export const getNextMonth = getMonthStateCreator(1);
export const getPreviousMonth = getMonthStateCreator(-1);

export const formatMonth = (day: dayjs.Dayjs) => ({
  month: day.month() + 1,
  year: day.year()
});

export const createCalendar = (yearMonth: YearMonthType) => {

  // 今月の最初の日を追加
  const firstDay = getMonth(yearMonth);
  const firstDayIndex = firstDay.day();

  return Array(35)
    .fill(0)
    .map((_, i) => {
      const diffFromFirstDay = i - firstDayIndex;
      const day = firstDay.add(diffFromFirstDay, "day");

      return day;
    });
};

export const getMonth = (yearMonth: YearMonthType) => {
  return dayjs(`${yearMonth.year}-${yearMonth.month}`);
};

export const isSameDay = (d1: dayjs.Dayjs, d2: dayjs.Dayjs) => {
  const format = "YYYYMMDD";
  return d1.format(format) === d2.format(format);
};

export const isSameMonth = (m1: dayjs.Dayjs, m2: dayjs.Dayjs) => {
  const format = "YYYYMM";
  return m1.format(format) === m2.format(format);
};

export const isFirstDay = (day: dayjs.Dayjs) => day.date() === 1;
