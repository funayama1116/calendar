import dayjs from "dayjs";
import { isSameDay } from "./calendar";

export const setSchedules = (calendar: Array<any>, schedules: any): Array<{date: any, schedules: any}> =>
  calendar.map(c => ({
    date: c,
    schedules: schedules.filter((e: any) => isSameDay(e.date, c))
  }));

export const formatSchedule = (schedule: any) => ({
  ...schedule,
  date: dayjs(schedule.date)
});

const isScheduleEmpty = (schedule: any) =>
  !schedule.title && !schedule.description && !schedule.location;

export const isCloseDialog = (schedule: any) => {
  const message = "保存されていない変更を破棄しますか？";

  return isScheduleEmpty(schedule) || window.confirm(message);
};