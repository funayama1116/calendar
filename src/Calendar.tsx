import React from 'react';
import { MuiPickersUtilsProvider } from "@material-ui/pickers";
import DayjsUtils from "@date-io/dayjs";

import dayjs from 'dayjs'
import "dayjs/locale/ja";

import Navigation from './component/Navigation/presentation'
// import {PickerSample} from './sample'

dayjs.locale("ja");

const Calender = () => {
    return (
        <>
            <MuiPickersUtilsProvider utils={DayjsUtils}>
                <Navigation />
                {/* <PickerSample /> */}
            </MuiPickersUtilsProvider>
        </>
    );

}

export default Calender;