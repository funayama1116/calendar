import React from 'react';
import ReactDOM from 'react-dom';
import Calender from './Calendar'

let dom = document.getElementById('root');
ReactDOM.render(
    <div>
        <Calender />
    </div>,
    dom
);