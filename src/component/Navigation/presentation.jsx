import React, { useReducer } from "react";
import { IconButton, Toolbar, Typography, withStyles, Tooltip } from "@material-ui/core";
import ArrowBackIos from "@material-ui/icons/ArrowBackIos";
import ArrowForwardIos from "@material-ui/icons/ArrowForwardIos";
import CalendarToday from '@material-ui/icons/CalendarToday'
import DehazeIcon from "@material-ui/icons/Dehaze";
import { DatePicker } from "@material-ui/pickers";

import CalendarBoard from '../CalendarBoard/presentation'

import calendarReducer, { statusInit } from '../../reducer/calendar/reducer'
import { 
  calendarSetMonth,
  calendarPreviousMonth,
  calendarCurrentMonth,
  calendarNextMonth
} from '../../reducer/calendar/actions'

const StyledToolbar = withStyles({
  root: { padding: "0" }
})(Toolbar);

const StyledTypography = withStyles({
  root: { margin: "0 30px 0 10px" }
})(Typography);

const StyledDatePicker = withStyles({
  root: { marginLeft: 30 }
})(DatePicker);

// const Navigation = ({ setNextMonth, setPreviousMonth, setMonth, month }) => {
const Navigation = () => {

  const [state, dispatch] = useReducer(calendarReducer, statusInit)
  console.debug("Component Navication")
  console.debug(state)

  return (
    <>
      <StyledToolbar>
        <IconButton>
          <DehazeIcon />
        </IconButton>
        <img alt="Calendar" src="/images/calendar_icon.png" width="30" height="30" />
        <StyledTypography color="textSecondary" variant="h5" component="h1">
          Calendar
        </StyledTypography>

        <Tooltip title="前の月" placement="bottom">
          <IconButton size="small" onClick={e => dispatch(calendarPreviousMonth())}>
            <ArrowBackIos />
          </IconButton>
        </Tooltip>
        <Tooltip title="今日" placement="bottom">
          <IconButton size="small" onClick={e => dispatch(calendarCurrentMonth())}>
            <CalendarToday />
          </IconButton>
        </Tooltip>
        <Tooltip title="次の月" placement="bottom">
          <IconButton size="small" onClick={e => dispatch(calendarNextMonth())}>
            <ArrowForwardIos />
          </IconButton>
        </Tooltip>
        <StyledDatePicker
          value={state.selectedDate}
          onChange={d => dispatch(calendarSetMonth({selectedDate: d}))}
          variant="inline"
          format="YYYY年M月"
          animateYearScrolling
          disableToolbar
        />
      </StyledToolbar>
      <CalendarBoard navigationStatus={state} />
    </>
  );
};

export default Navigation;