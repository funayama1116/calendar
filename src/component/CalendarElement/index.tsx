import React from "react";
import './style.css';
import { Typography } from "@material-ui/core";
import dayjs from "dayjs";
import {
  isSameDay,
  isSameMonth,
  isFirstDay
} from "../../services/calendar";
import Schedule from "../Schedule";
import { scheduleType } from "../Schedule/entity"

// dayという変数にprops.childrenが代入される
// const CalendarElement = ({ day, month, schedules, ...props }) => {
const CalendarElement = (
  { date, today, selectedDate, schedules, addScheduleDispatch }: 
  { date: dayjs.Dayjs, today: dayjs.Dayjs, selectedDate: dayjs.Dayjs, schedules: Array<scheduleType>, addScheduleDispatch: Function }
  ) => {

  console.debug("Component CalendarElement: " + date.toISOString())

  // 今月以外をグレーダウン
  const isCurrentMonth = isSameMonth(date, selectedDate);
  const textColor = isCurrentMonth ? "textPrimary" : "textSecondary";

  // 文字列のフォーマットをどうするか
  // 月の最初だけ月情報をつける
  const format = isFirstDay(date) ? "M月D日" : "D";

  // 当日かどうか判断
  const isToday = isSameDay(date, today);

  return (
    <div className="element">
      <Typography
        className="date"
        color={textColor}
        align="center"
        variant="caption"
        component="div"
      >
        <span className={isToday ? "today" : ""}>
          {date.format(format)}
        </span>
      </Typography>
      <div className="schedules">
        {/* <Schedule /> */}
        {schedules.map((item: scheduleType) => (
          <Schedule key={item.id} schedule={item} addScheduleDispatch={addScheduleDispatch} />
        ))}
      </div>
    </div>
  );
};

export default CalendarElement;
