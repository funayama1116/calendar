import React from "react";
import './style.css';
import { scheduleType } from "./entity"
import {
  addScheduleOpenDialog,
  addScheduleSetValue
} from '../../reducer/addSchedule/actions'

// const Schedule = ({ schedule, onClickSchedule }) => {
const Schedule = (
  { schedule, addScheduleDispatch }:
  { schedule: scheduleType, addScheduleDispatch: Function }
  ) => {

    console.debug("Component Schedule: " + schedule.title)

    return (
      <div
        className="schedule"
        onClick={e => {
          addScheduleDispatch(addScheduleSetValue(schedule));
          addScheduleDispatch(addScheduleOpenDialog({date: schedule.date}));
        }}
      >
        {schedule.title}
      </div>
  );
};

export default Schedule;