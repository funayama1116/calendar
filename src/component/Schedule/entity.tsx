import dayjs from "dayjs"
export type scheduleType = {
    id: string,
    title: string,
    location: string,
    description: string,
    date: dayjs.Dayjs
}
