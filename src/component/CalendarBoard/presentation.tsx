import React, { useReducer } from "react";
import dayjs from "dayjs";

import { createCalendar, formatMonth } from "../../services/calendar";
import {
  GridList,
  Typography
} from "@material-ui/core";
import { 
  StatusType as NavigationStatusType,
  statusInit as navigationStatusInit
} from '../../reducer/calendar/reducer'
import CalendarElement from "../CalendarElement";
import './style.css';

import AddScheduleDialog from '../AddScheduleDialog/presentation'
import addScheduleReducer, { statusInit as addScheduleStatusInit } from '../../reducer/addSchedule/reducer'
import {
  addScheduleOpenDialog,
} from '../../reducer/addSchedule/actions'

import { setSchedules } from "../../services/schedule"

const days = ["日", "月", "火", "水", "木", "金", "土"];

// const CalendarBoard = ({
//   calendar,
//   month,
//   openAddScheduleDialog,
//   openCurrentScheduleDialog,
//   fetchSchedule
// }) => {
const CalendarBoard = (
  {navigationStatus= navigationStatusInit}: 
  {navigationStatus: NavigationStatusType}
  ) => {
  
  console.debug("Component CalendarBoard")
  // useEffect(() => {
  //   // 初回のみdataを取得する
  //   fetchSchedule();
  // }, []);

  const [addScheduleState, addScheduleDispatch] = useReducer(addScheduleReducer, addScheduleStatusInit)

  const schedules = [
    {
      id: "1",
      title: "title 1",
      date: dayjs(),
      location: "location 1",
      description: "this is description 1"
    },
    {
      id: "2",
      title: "title 2",
      date: dayjs().add(1, "day"),
      location: "location 2",
      description: "this is description 2"
    },
    {
      id: "3",
      title: "title 3",
      date: dayjs().add(1, "month"),
      location: "location 3",
      description: "this is description 3"
    }
  ]

  const calendar = setSchedules(
    createCalendar(formatMonth(navigationStatus.selectedDate)),
    schedules
  )
  
  return (
    <>
      <div className="container">
        <GridList className="grid" cols={7} spacing={0} cellHeight="auto">
          {days.map(d => (
            <li key={d}>
              <Typography
                className="days"
                color="textSecondary"
                align="center"
                variant="caption"
                component="div"
              >
                {d}
              </Typography>
            </li>
          ))}
          { calendar.map(({ date, schedules }) => (
            <li
              key={date.toISOString()}
              onClick={() => {addScheduleDispatch(addScheduleOpenDialog({date: date}))}}
            >
              <CalendarElement
                date={date}
                {...navigationStatus}
                schedules={schedules}
                // onClickSchedule={() => {addScheduleDispatch(addScheduleOpenDialog({date: date}))}}
                addScheduleDispatch={addScheduleDispatch}
              />
            </li>
          ))}
        </GridList>
      </div>
      <AddScheduleDialog 
        dispatchFunction={addScheduleDispatch} 
        state={addScheduleState}
        />
    </>
  );
};

export default CalendarBoard;